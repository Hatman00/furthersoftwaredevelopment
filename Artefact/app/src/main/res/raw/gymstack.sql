DROP DATABASE IF EXISTS gymStack;
CREATE DATABASE gymStack;
USE gymStack;
DROP TABLE IF EXISTS table_users;
CREATE TABLE table_users(
	user_id INT NOT NULL,
    user_name varchar(255),
    user_pass INT NOT NULL,
    user_phone varchar(255),
    user_class_amnt int,
    user_pref_sport varchar(255),
    user_pref_class varchar(255),
    user_staff bool,
    PRIMARY KEY(user_id)
);
DROP TABLE IF EXISTS table_gyms;
CREATE TABLE table_gyms(
	gym_id INT NOT NULL,
    gym_name varchar(255),
    gym_location varchar(255),
    gym_cost int,
    gym_time_open int,
    gym_time_close int,
    PRIMARY KEY(gym_id)
);
DROP TABLE IF EXISTS table_classes;
CREATE TABLE table_classes(
	class_id INT NOT NULL,
    class_name varchar(255),
    class_cost int,
    class_date varchar(255),
    class_time int,
    class_capacity int,
    class_amount int,
    PRIMARY KEY(class_id)
);
DROP TABLE IF EXISTS link_gym_user;
CREATE TABLE link_gym_user(
	gym_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY(gym_id, user_id),
    FOREIGN KEY(gym_id) REFERENCES table_gyms(gym_id),
    FOREIGN KEY(user_id) REFERENCES table_users(user_id)
);
DROP TABLE IF EXISTS link_gym_classes;
CREATE TABLE link_gym_classes(
	gym_id INT NOT NULL,
    class_id INT NOT NULL,
    PRIMARY KEY(gym_id, class_id),
    FOREIGN KEY(gym_id) REFERENCES table_gyms(gym_id),
    FOREIGN KEY(class_id) REFERENCES table_classes(class_id)
);
DROP TABLE IF EXISTS link_class_user;
CREATE TABLE link_class_user(
	class_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY(class_id, user_id),
    FOREIGN KEY(class_id) REFERENCES table_classes(class_id),
    FOREIGN KEY(user_id) REFERENCES table_users(user_id)
);
package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class GymSearch extends AppCompatActivity {

    String username;
    String callingButtonID;
    DBHandler handler;
    Context gymSearchContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym_search);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("OnCreate", username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        gymSearchContext = this;
    }

    public void searchSwitch(View view){
        callingButtonID = String.valueOf(getResources().getResourceEntryName(view.getId()));
        ConstraintLayout startConstraint = findViewById(R.id.app_gymsearch_constraint_main);
        ConstraintLayout endConstraintt = findViewById(R.id.app_gymsearch_constraint_searchResult);
        LinearLayout searchLinear = findViewById(R.id.app_gymsearch_linear_searchScroll);
        if (callingButtonID.equals("app_gymsearch_button_search")){
            Log.d("searchSwitch", "User clicked search button");
            EditText searchBox = findViewById(R.id.app_gymsearch_entry_search);
            if (!String.valueOf(searchBox.getText()).equals("")) {
                String[] searchResults = handler.searchGyms(String.valueOf(searchBox.getText()));
                if (searchResults.length > 0) {
                    Log.d("searchSwitch", "Search results found");
                    Log.d("searchSwitch", "Search amount is " + String.valueOf(searchResults.length));
                    ScrollView searchScroll = findViewById(R.id.app_gymsearch_scroll_searchList);
                    ViewGroup.LayoutParams searchLinearParams = searchLinear.getLayoutParams();
                    ViewGroup.LayoutParams searchScrollParams = searchScroll.getLayoutParams();
                    Button[] searchButtons = new Button[searchResults.length];
                    String buttonText = "";
                    String[] gymSearchSplit = new String[2];
                    int x = 0;
                    while (x < searchResults.length) {
                        Log.d("searchSwitch", "x is " + String.valueOf(x));
                        Log.d("searchSwitch", "Current gym is " + searchResults[x]);
                        gymSearchSplit = searchResults[x].split("/");
                        Log.d("searchSwitch", "gymSearchSplit is " + gymSearchSplit[0] + "\n" + gymSearchSplit[1]);
                        buttonText = gymSearchSplit[0];
                        searchButtons[x] = new Button(this);
                        searchButtons[x].setHeight(50);
                        searchButtons[x].setWidth(50);
                        searchButtons[x].setText(buttonText);
                        searchButtons[x].setId(Integer.parseInt(gymSearchSplit[1]));
                        if (handler.checkIndividualGymMembership(username, gymSearchSplit[1])) {
                            searchButtons[x].setBackgroundColor(Color.rgb(120, 81, 169));
                        } else {
                            searchButtons[x].setBackgroundColor(Color.rgb(0, 0, 238));
                        }
                        searchButtons[x].setTextColor(Color.WHITE);
                        searchButtons[x].setOnClickListener(gymButtonListener);
                        LinearLayout.LayoutParams searchButtonsLayout = new LinearLayout.LayoutParams(800, 300);
                        searchButtonsLayout.setMargins(0, 0, 0, 50);
                        searchButtons[x].setLayoutParams(searchButtonsLayout);
                        searchLinear.addView(searchButtons[x]);
                        searchLinear.setLayoutParams(searchLinearParams);
                        searchScroll.setLayoutParams(searchScrollParams);
                        x++;
                    }
                } else {
                    Log.d("searchSwitch", "No search results given, displaying error");
                    TextView noResults = new TextView(this);
                    noResults.setText(getString(R.string.app_gymsearch_textview_noResults));
                    noResults.setGravity(Gravity.CENTER);
                    noResults.setTextColor(Color.RED);
                    noResults.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
                    searchLinear.addView(noResults);
                }
            }
            else{
                Log.d("searchSwitch", "Search box is empty, showing error");
                searchBox.setError(getString(R.string.app_gymsearch_textview_emptySearchError));
                searchBox.requestFocus();
                return;
            }
        }
        else{
            Log.d("searchSwitch", "The back button was pressed");
            if (searchLinear.getChildCount() > 0){
                Log.d("searchSwitch", "searchLinear has views, removing");
                searchLinear.removeAllViews();
            }
        }
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        constraintSwitch(view, startConstraint, endConstraintt);
    }

    public void constraintSwitch(View view, ConstraintLayout constraint1, ConstraintLayout constraint2){
        Log.d("ConstraintSwitch", "Start");
        if (constraint1.getVisibility() == view.GONE){
            Log.d("ConstraintSwitch", "If Statement");
            constraint1.setVisibility(view.VISIBLE);
            constraint2.setVisibility(view.GONE);
        }
        else{
            Log.d("ConstraintSwitch", "Else Statement");
            constraint1.setVisibility(view.GONE);
            constraint2.setVisibility(view.VISIBLE);
        }
    }

    View.OnClickListener gymButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent gymActivitySwitch = new Intent(gymSearchContext, GymActivity.class);
            gymActivitySwitch.putExtra("username", username);
            gymActivitySwitch.putExtra("gymID", String.valueOf(view.getId()));
            startActivity(gymActivitySwitch);
            finish();
        }
    };

    public void backButton(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}
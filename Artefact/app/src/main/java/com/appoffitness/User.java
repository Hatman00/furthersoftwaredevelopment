package com.appoffitness;

public class User {
    private String userName;
    private String userPass;
    private String userEmail;
    private String userPhone;
    private String userPrefClass;
    private String userPrefSport;

    public User(String userName, String userPass, String userEmail, String userPhone, String userPrefClass, String userPrefSport, Boolean userAuthed){
        this.userName = userName;
        this.userPass = userPass;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.userPrefClass = userPrefClass;
        this.userPrefSport = userPrefSport;
    }

    public String getUserName(){
        return this.userName;
    }

    public String getUserPass(){
        return this.userPass;
    }

    public String getUserEmail(){
        return this.userEmail;
    }

    public String getUserPhone(){
        return this.userPhone;
    }

    public String getUserPrefClass(){
        return this.userPrefClass;
    }

    public String getUserPrefSport(){
        return this.userPrefSport;
    }
}

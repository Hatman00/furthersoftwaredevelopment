package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String user = sp.getString("username", "na");
        Log.d("MainOnCreate", user);
        if (handler.userExists(user, null)){
            Intent loginSwitch = new Intent(this, HomeActivity.class);
            loginSwitch.putExtra("username", user);
            loginSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginSwitch);
            finish();
        }
    }

    public void loginSwitch(View view){
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
    }

    public void signupSwitch(View view){
        Intent signupIntent = new Intent(this, SignupActivity.class);
        startActivity(signupIntent);
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d("STOP!", "I HAVE STOPPED!");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        String user = sp.getString("username", "na");
        Log.d("MAYONAISE IS AWFUL", user);
    }
}
package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class GymActivity extends AppCompatActivity {

    String username;
    String callingButtonID;
    String gymID;
    String classID;
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym);
        Intent intentGetExtra = getIntent();
        username = intentGetExtra.getStringExtra("username");
        try{
            gymID = intentGetExtra.getStringExtra("gymID");
            Log.d("GAOnCreate", "gymID sourced from intent and is " + gymID);
        }
        catch (Exception error){
            Log.d("GAOnCreate", "gymID not sourced, running as normal");
        }
        Log.d("GAOnCreate", "Username is " + username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        if (gymID != null && !gymID.equals("")){
            Log.d("GAOnCreate", "gymID sourced, interrupting normal operation");
            View meh = new Button(this);
            meh.setOnClickListener(gymButtonListener);
            meh.setId(Integer.parseInt(gymID));
            meh.performClick();
        }
        listGyms();

    }

    public void backButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    public void listGyms(){
        String[] gymList = handler.getGyms();
        String buttonText = "";
        LinearLayout gymLinear = findViewById(R.id.app_gym_linear_scroll);
        ScrollView gymScroll = findViewById(R.id.app_gym_scroll_gymList);
        ViewGroup.LayoutParams gymLinearParams = gymLinear.getLayoutParams();
        ViewGroup.LayoutParams gymScrollParams = gymScroll.getLayoutParams();
        Button[] gymButtons = new Button[gymList.length];
        int x = 0;
        Log.d("listGyms", "gymList length " + String.valueOf(gymList.length));
        while (x < gymList.length){
            Log.d("listGyms", "Current gym is " + String.valueOf(gymList[x]));
            buttonText = String.valueOf(gymList[x]);
            gymButtons[x] = new Button(this);
            gymButtons[x].setHeight(50);
            gymButtons[x].setWidth(50);
            gymButtons[x].setTextColor(Color.BLACK);
            gymButtons[x].setText(buttonText);
            gymButtons[x].setId(x + 1);
            gymButtons[x].setOnClickListener(gymButtonListener);
            if (handler.checkIndividualGymMembership(username, String.valueOf(x + 1))){
                gymButtons[x].setBackgroundColor(Color.rgb(120, 81, 169));
            }
            else{
                gymButtons[x].setBackgroundColor(Color.rgb(0, 0, 238));
            }
            gymButtons[x].setTextColor(Color.WHITE);
            LinearLayout.LayoutParams gymButtonLayout = new LinearLayout.LayoutParams(800, 300);
            gymButtonLayout.setMargins(0, 0, 0, 50);
            gymButtons[x].setLayoutParams(gymButtonLayout);
            gymLinear.addView(gymButtons[x]);
            gymLinear.setLayoutParams(gymLinearParams);
            gymScroll.setLayoutParams(gymScrollParams);
            x++;
            Log.d("listGyms", "x is " + String.valueOf(x));
        }
    }

    public void gymViewSwitch(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_gym_constraint_main);
        ConstraintLayout endConstraint = findViewById(R.id.app_gym_constraint_gymView);
        constraintSwitch(view, startConstraint, endConstraint);
    }

    public void classViewSwitch(View view){
        callingButtonID = String.valueOf(getResources().getResourceEntryName(view.getId()));
        ConstraintLayout startConstraint = findViewById(R.id.app_gym_constraint_gymView);
        ConstraintLayout endConstraint = findViewById(R.id.app_gym_constraint_classView);
        Log.d("classViewSwitch", "callingButtonID is " + callingButtonID);
        if (callingButtonID.equals("app_gym_button_classes")){
            Log.d("classViewSwitch", "User clicked classes button");
            String[] classList = handler.getIndividualGymClasses(gymID);
            if (classList.length > 0) {
                Log.d("classViewSwitch", "Classes found, converting to buttons");
                String buttonText = "";
                LinearLayout classLinear = findViewById(R.id.app_gym_linear_ClassScroll);
                if (classLinear.getChildCount() > 0){
                    Log.d("classViewSwitch", "Scroll linear already has children, removing");
                    classLinear.removeAllViews();
                }
                ScrollView classScroll = findViewById(R.id.app_gym_scroll_classList);
                ViewGroup.LayoutParams classLinearParams = classLinear.getLayoutParams();
                ViewGroup.LayoutParams classScrollParams = classScroll.getLayoutParams();
                Button[] classButtons = new Button[classList.length];
                int x = 0;
                Log.d("classViewSwitch", "classList length is " + String.valueOf(classList.length));
                while (x < classList.length){
                    Log.d("classViewSwitch", "Current class is " + classList[x]);
                    buttonText = String.valueOf(classList[x]);
                    classButtons[x] = new Button(this);
                    classButtons[x].setHeight(50);
                    classButtons[x].setWidth(50);
                    classButtons[x].setText(buttonText);
                    classButtons[x].setId(handler.getClassID(buttonText));
                    if (handler.checkIndividualClassMembership(username, String.valueOf(handler.getClassID(buttonText)))){
                        classButtons[x].setBackgroundColor(Color.rgb(120, 81, 169));
                    }
                    else{
                        classButtons[x].setBackgroundColor(Color.rgb(0, 0, 238));
                    }
                    classButtons[x].setTextColor(Color.WHITE);
                    classButtons[x].setOnClickListener(classButtonListener);
                    LinearLayout.LayoutParams classButtonLayout = new LinearLayout.LayoutParams(800, 300);
                    classButtonLayout.setMargins(0, 0, 0, 50);
                    classButtons[x].setLayoutParams(classButtonLayout);
                    classLinear.addView(classButtons[x]);
                    classLinear.setLayoutParams(classLinearParams);
                    classScroll.setLayoutParams(classScrollParams);
                    x++;
                    Log.d("classViewSwitch", "x is " + String.valueOf(x));
                }
            }
            else{
                Log.d("classViewSwitch", "No classes found, showing error");
                LinearLayout classLinear = findViewById(R.id.app_gym_linear_ClassScroll);
                if (classLinear.getChildCount() > 0){
                    Log.d("classViewSwitch", "Scroll linear already has children, removing");
                    classLinear.removeAllViews();
                }
                TextView noClasses = new TextView(this);
                noClasses.setText(getString(R.string.app_gym_textview_noClasses));
                noClasses.setGravity(Gravity.CENTER);
                noClasses.setTextColor(Color.RED);
                noClasses.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30); //https://stackoverflow.com/questions/6998938/textview-setting-the-text-size-programmatically-doesnt-seem-to-work
                classLinear.addView(noClasses);
            }
        }
        else{
            Log.d("classViewSwitch", "User did not click classes button");
        }
        constraintSwitch(view, startConstraint, endConstraint);
    }

    public void classInfoSwitch(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_gym_constraint_classView);
        ConstraintLayout endConstraint = findViewById(R.id.app_gym_constraint_classInfo);
        constraintSwitch(view, startConstraint, endConstraint);
    }

    View.OnClickListener gymButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            StringBuilder timeFormat;
            String gymInfoJoined = "";
            callingButtonID = String.valueOf(view.getId());
            String[] gymInfo = handler.getGymInfo(callingButtonID);
            TextView gymName = findViewById(R.id.app_gym_textview_gymName);
            TextView gymText = findViewById(R.id.app_gym_textview_gymText);
            Button membershipButton = findViewById(R.id.app_gym_button_membership);
            gymName.setText(gymInfo[0]);
            gymInfoJoined = "Location: " + gymInfo[1];
            gymInfoJoined = gymInfoJoined + "\n\nCost Per Month: £" + gymInfo[2];
            Log.d("onClickListener", String.valueOf(gymInfo[3].charAt(1)));
            if (String.valueOf(gymInfo[3].charAt(0)).equals("0") && !gymInfo[3].equals("0000")){
                gymInfo[3] = gymInfo[3].substring(1);
                timeFormat = new StringBuilder(gymInfo[3]);
                timeFormat.insert(1, ":");
            }
            else{
                timeFormat = new StringBuilder(gymInfo[3]);
                timeFormat.insert(2, ":");
            }
            gymInfoJoined = gymInfoJoined + "\n\nOpening Time: " + String.valueOf(timeFormat);
            if (String.valueOf(gymInfo[4].charAt(0)).equals("0") && !gymInfo[4].equals("0000")){
                gymInfo[4] = gymInfo[4].substring(1);
                timeFormat = new StringBuilder(gymInfo[4]);
                timeFormat.insert(2, ":");
            }
            else{
                timeFormat = new StringBuilder(gymInfo[4]);
                timeFormat.insert(2, ":");
            }
            gymInfoJoined = gymInfoJoined + "\n\nClosing Time: " + String.valueOf(timeFormat);
            Log.d("onClickListener", "gymInfoJoin = " + gymInfoJoined);
            gymText.setText(gymInfoJoined);
            gymID = callingButtonID;
            if (handler.checkIndividualGymMembership(username, gymID)){
                membershipButton.setBackgroundColor(Color.RED);
                membershipButton.setTextColor(Color.WHITE);
                membershipButton.setText(getString(R.string.app_gym_button_gymLeave));
            }
            else{
                membershipButton.setBackgroundColor(Color.GREEN);
                membershipButton.setTextColor(Color.BLACK);
                membershipButton.setText(getString(R.string.app_gym_button_gymJoin));
            }
            gymViewSwitch(view);
        }
    };

    View.OnClickListener classButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("classOnClickListener", "User clicked a class button");
            StringBuilder timeFormat;
            String classInfoJoined = "";
            callingButtonID = String.valueOf(view.getId());
            Log.d("classOnClickListener", "callingButtonID is " + callingButtonID);
            String[] classInfo = handler.getClassInfo(callingButtonID);
            TextView className = findViewById(R.id.app_gym_textview_className);
            TextView classText = findViewById(R.id.app_gym_textview_classText);
            Button classMembershipButton = findViewById(R.id.app_gym_button_classJoin);
            className.setText(classInfo[0]);
            classInfoJoined = classInfoJoined + "\n\nCost: £" + classInfo[1];
            if (String.valueOf(classInfo[2].charAt(0)).equals("0") && !classInfo[2].equals("0000")){
                classInfo[2] = classInfo[2].substring(1);
                timeFormat = new StringBuilder(classInfo[2]);
                timeFormat.insert(1, ":");
            }
            else{
                timeFormat = new StringBuilder(classInfo[2]);
                timeFormat.insert(2, ":");
            }
            classInfoJoined = classInfoJoined + "\n\nTime: " + String.valueOf(timeFormat);
            classInfoJoined = classInfoJoined + "\n\nCapacity: " + classInfo[3];
            classInfoJoined = classInfoJoined + "\n\nSpaces Left: " + String.valueOf(Integer.parseInt(classInfo[3]) - Integer.parseInt(classInfo[4]));
            Log.d("classOnClickListener", "classInfoJoined is " + classInfoJoined);
            classText.setText(classInfoJoined);
            classID = callingButtonID;
            Log.d("classOnClickListener", "Checking if user has a link to class or if the class is full");
            if (handler.checkIndividualClassMembership(username, classID)){
                Log.d("classOnClickListener", "User is already a member of this class");
                classMembershipButton.setBackgroundColor(Color.RED);
                classMembershipButton.setTextColor(Color.WHITE);
                classMembershipButton.setText(getString(R.string.app_gym_button_classLeave));
                classMembershipButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            }
            else if (!handler.checkClassSpace(classID)){
                Log.d("classOnClickListener", "This class has no space");
                classMembershipButton.setBackgroundColor(Color.RED);
                classMembershipButton.setTextColor(Color.WHITE);
                classMembershipButton.setText(getText(R.string.app_gym_button_classFull));
                classMembershipButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            }
            else{
                Log.d("classOnClickListener", "This class is not full and the user is not a member of it");
                classMembershipButton.setBackgroundColor(Color.GREEN);
                classMembershipButton.setTextColor(Color.BLACK);
                classMembershipButton.setText(getString(R.string.app_gym_button_classJoin));
                classMembershipButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 48);
            }
            classInfoSwitch(view);
        }
    };

    public void gymMembershipButton(View view){
        Button gymButton = findViewById(Integer.parseInt(gymID));
        if (handler.setGymMembership(username, gymID)){
            Toast.makeText(this, "You are now a member of the gym", Toast.LENGTH_LONG).show();
            gymButton.setBackgroundColor(Color.rgb(120, 81, 169));
        }
        else{
            Toast.makeText(this, "You are no longer a member of the gym", Toast.LENGTH_LONG).show();
            gymButton.setBackgroundColor(Color.rgb(0, 0, 238));
        }
        gymViewSwitch(view);
    }

    public void classMembershipButton(View view){
        Button classMembershipButton = findViewById(R.id.app_gym_button_classJoin);
        Button classButton = findViewById(Integer.parseInt(classID));
        Log.d("classMembershipButton", String.valueOf(classMembershipButton.getText()));
        if (classMembershipButton.getText().equals(getString(R.string.app_gym_button_classJoin))){
            Log.d("classMembershipButton", "Membership button is join");
            if (handler.checkClassSpace(classID)){
                Log.d("classMembershipButton", "Class has space for user. Creating link");
                handler.setClassMemebership(username, classID, true);
                handler.addUserHistory(username, classID);
                Toast.makeText(this, "You are now RSVPed for this class", Toast.LENGTH_LONG).show();
                classButton.setBackgroundColor(Color.rgb(120, 81, 169));
                classInfoSwitch(view);
            }
            else{
                Log.d("classMembershipButton", "Class has no for user. No link created");
                Toast.makeText(this, "This class is full", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Log.d("classMembershipButton", "Membership is leave");
            Log.d("classMembershipButton", "Removing link");
            handler.setClassMemebership(username, classID, false);
            Toast.makeText(this, "Your RSVP for this class has been canceled", Toast.LENGTH_LONG).show();
            classButton.setBackgroundColor(Color.rgb(0, 0, 238));
            classInfoSwitch(view);
        }
    }

    public void constraintSwitch(View view, ConstraintLayout constraint1, ConstraintLayout constraint2){
        Log.d("ConstraintSwitch", "Start");
        if (constraint1.getVisibility() == view.GONE){
            Log.d("ConstraintSwitch", "If Statement");
            constraint1.setVisibility(view.VISIBLE);
            constraint2.setVisibility(view.GONE);
        }
        else{
            Log.d("ConstraintSwitch", "Else Statement");
            constraint1.setVisibility(view.GONE);
            constraint2.setVisibility(view.VISIBLE);
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}
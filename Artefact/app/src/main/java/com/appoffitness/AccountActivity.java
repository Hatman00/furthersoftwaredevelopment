package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AccountActivity extends AppCompatActivity {

    String username;
    String callingButtonID;
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Intent accountIntent = getIntent();
        username = accountIntent.getStringExtra("username");
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }

    public void settingsSwitch(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_main);
        ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_settings);
        constraintSwitch(view, startConstraint, endConstraint);
    }

    public void uInfoSwitch(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_main);
        ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_uInfo);
        Log.d("uInfoSwitch", String.valueOf(getResources().getResourceEntryName(view.getId())));
        if (String.valueOf(getResources().getResourceEntryName(view.getId())).equals("app_account_button_uInfo")){
            TextView uInfo = findViewById(R.id.app_account_textview_uInfoContent);
            uInfo.setText(handler.getUserInfo(username));
        }
        constraintSwitch(view, startConstraint, endConstraint);
    }

    public void preferenceSwitch(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_main);
        ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_preferences);
        constraintSwitch(view, startConstraint, endConstraint);
    }

    public void preferenceChangeSwitch(View view){
        callingButtonID = String.valueOf(getResources().getResourceEntryName(view.getId()));
        ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_preferences);
        ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cPref);
        constraintSwitch(view, startConstraint, endConstraint);
        TextView cPrefWelcome = findViewById(R.id.app_account_textview_cPrefSelectWelcome);
        String welcomeMessage = getString(R.string.app_account_textview_selectPrefWelcome);
        Spinner dropDown = findViewById(R.id.app_account_spinner_prefSelect);
        if (callingButtonID.equals("app_account_button_cClass")){
            welcomeMessage = welcomeMessage + " class:";
            cPrefWelcome.setText(welcomeMessage);
            String[] dropDownValues = handler.getTypes(false);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dropDownValues);
            dropDown.setAdapter(adapter);
        }
        if (callingButtonID.equals("app_account_button_cSport")){
            welcomeMessage = welcomeMessage + " sport:";
            cPrefWelcome.setText(welcomeMessage);
            String[] dropDownValues = handler.getTypes(true);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dropDownValues);
            dropDown.setAdapter(adapter);
        }
    }

    public void preferenceChangeButton(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_preferences);
        ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cPref);
        Spinner dropDown = findViewById(R.id.app_account_spinner_prefSelect);
        Log.d("preferenceChangeButton", callingButtonID);
        if (callingButtonID.equals("app_account_button_cClass")){
            handler.modifyUserInfo(4, username, dropDown.getSelectedItem().toString());
            Toast.makeText(this, "Changed Preferred Class", Toast.LENGTH_LONG).show();
            Log.d("preferenceChangeButton", "Changed User Preferred Class");
        }
        if (callingButtonID.equals("app_account_button_cSport")){
            handler.modifyUserInfo(5, username, dropDown.getSelectedItem().toString());
            Toast.makeText(this, "Changed Preferred Sport", Toast.LENGTH_LONG).show();
            Log.d("preferenceChangeButton", "Changed User Preferred Sport");
        }
        constraintSwitch(view, startConstraint, endConstraint);
    }

    public void changeUserInfoSwitch(View view){
        callingButtonID = String.valueOf(getResources().getResourceEntryName(view.getId()));
        ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_settings);
        ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cValues);
        constraintSwitch(view, startConstraint, endConstraint);
        TextView cValuesWelcome = findViewById(R.id.app_account_textview_cValuesWelcome);
        EditText textBox = findViewById(R.id.app_account_entry_nValue);
        textBox.setText("");
        String welcomeMessage = getString(R.string.app_account_textview_cValues);
        if (callingButtonID.equals("app_account_button_cUser")){
            welcomeMessage = welcomeMessage + " username:";
            cValuesWelcome.setText(welcomeMessage);
        }
        if (callingButtonID.equals("app_account_button_cEmail")){
            welcomeMessage = welcomeMessage + " Email:";
            cValuesWelcome.setText(welcomeMessage);
        }

        if (callingButtonID.equals("app_account_button_cPhone")){
            welcomeMessage = welcomeMessage + " phone number:";
            cValuesWelcome.setText(welcomeMessage);
        }
        if (callingButtonID.equals("app_account_button_cPassword")){
            welcomeMessage = welcomeMessage + " password:";
            cValuesWelcome.setText(welcomeMessage);
        }
        if (callingButtonID.equals("app_account_button_cValuesBack")){
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        Log.d("changeSwitch", callingButtonID);
    }

    public void changeButton(View view){
        EditText newValueEntry = findViewById(R.id.app_account_entry_nValue);
        if (newValueEntry.equals("")){
            newValueEntry.setError("Please entry a new value!");
            newValueEntry.requestFocus();
            return;
        }
        else if (newValueEntry.getText().length() < 8 && callingButtonID.equals("app_account_button_cPassword")){
            Toast.makeText(this, "Password must be 8 or more characters!", Toast.LENGTH_LONG).show();
            return;
        }
        else{
            if (callingButtonID.equals("app_account_button_cUser")){
                if (!handler.userExists(String.valueOf(newValueEntry.getText()), null)) {
                    handler.modifyUserInfo(0, username, String.valueOf(newValueEntry.getText()));;
                    Toast.makeText(this, "Changed Username", Toast.LENGTH_LONG).show();
                    ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_settings);
                    ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cValues);
                    constraintSwitch(view, startConstraint, endConstraint);
                    username = String.valueOf(newValueEntry.getText());
                }
                else{
                    Toast.makeText(this, "Username already exists!", Toast.LENGTH_LONG).show();
                }
            }
            if (callingButtonID.equals("app_account_button_cEmail")){
                handler.modifyUserInfo(2, username, String.valueOf(newValueEntry.getText()));
                Toast.makeText(this, "Changed Password", Toast.LENGTH_LONG).show();
                ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_settings);
                ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cValues);
                constraintSwitch(view, startConstraint, endConstraint);
                Toast.makeText(this, "Changed Email", Toast.LENGTH_LONG).show();
            }
            if (callingButtonID.equals("app_account_button_cPhone")){
                handler.modifyUserInfo(3, username, String.valueOf(newValueEntry.getText()));
                Toast.makeText(this, "Changed Phone Number", Toast.LENGTH_LONG).show();
                ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_settings);
                ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cValues);
                constraintSwitch(view, startConstraint, endConstraint);
            }
            if (callingButtonID.equals("app_account_button_cPassword")){
                handler.modifyUserInfo(1, username, String.valueOf(newValueEntry.getText()));
                Toast.makeText(this, "Changed Password", Toast.LENGTH_LONG).show();
                ConstraintLayout startConstraint = findViewById(R.id.app_account_constraint_settings);
                ConstraintLayout endConstraint = findViewById(R.id.app_account_constraint_cValues);
                constraintSwitch(view, startConstraint, endConstraint);
            }
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void deleteUsers(View view){
        handler.deleteAccount(username);
        Log.d("LOGOUT", "LOGGING OUT");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Intent logoutSwitch = new Intent(this, MainActivity.class);
        logoutSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(logoutSwitch);
        finish();
    }

    public void historyButton(View view){
        Intent historySwitch = new Intent(this, ClassHistory.class);
        historySwitch.putExtra("username", username);
        startActivity(historySwitch);
    }

    public void backButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d("STOP!", "I HAVE STOPPED!");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("username", username);
        edit.apply();
    }

    public void constraintSwitch(View view, ConstraintLayout constraint1, ConstraintLayout constraint2){
        Log.d("ConstraintSwitch", "Start");
        if (constraint1.getVisibility() == view.GONE){
            Log.d("ConstraintSwitch", "If Statement");
            constraint1.setVisibility(view.VISIBLE);
            constraint2.setVisibility(view.GONE);
        }
        else{
            Log.d("ConstraintSwitch", "Else Statement");
            constraint1.setVisibility(view.GONE);
            constraint2.setVisibility(view.VISIBLE);
        }
    }
}
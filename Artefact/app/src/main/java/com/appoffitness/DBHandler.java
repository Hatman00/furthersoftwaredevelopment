package com.appoffitness;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.service.autofill.UserData;
import android.util.Log;
import android.widget.Toast;

import com.appoffitness.User;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DBHandler extends SQLiteOpenHelper {
    static SQLiteDatabase db;
    String uID;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "fitnessdata.db";
    private static final String TABLE_NAME = "table_users";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_EMAIL= "email";
    private static final String COLUMN_MOBILE = "mobile";
    private static final String COLUMN_PREFSPORT = "pref_sport";
    private static final String COLUMN_PREFCLASS = "pref_class";

    private static final String CREATE_TABLE_USERS = "CREATE TABLE IF NOT EXISTS table_users" +
            "(user_id integer PRIMARY KEY NOT NULL," +
            "username text NOT NULL," +
            "password text NOT NULL," +
            "email text NOT NULL," +
            "mobile text," +
            "pref_sport text," +
            "pref_class text);";

    private static final String CREATE_TABLE_GYMS = "CREATE TABLE IF NOT EXISTS table_gyms" +
            "(gym_id integer PRIMARY KEY NOT NULL," +
            "gym_name text NOT NULL," +
            "gym_location text NOT NULL," +
            "gym_cost integer," +
            "gym_time_open text NOT NULL," +
            "gym_time_close text NOT NULL);";

    private static final String CREATE_TABLE_CLASSES = "CREATE TABLE IF NOT EXISTS table_classes" +
            "(class_id integer PRIMARY KEY NOT NULL," +
            "class_name text NOT NULL," +
            "class_cost integer," +
            "class_time text NOT NULL," +
            "class_capacity integer NOT NULL," +
            "class_amount integer NOT NULL);";

    private static final String CREATE_TABLE_TYPES = "CREATE TABLE IF NOT EXISTS table_types" +
            "(type_id integer PRIMARY KEY NOT NULL," +
            "type_name text NOT NULL," +
            "type_isClass bool NOT NULL," +
            "type_isSport bool NOT NULL);";

    private static final String CREATE_TABLE_HISTORY = "CREATE TABLE IF NOT EXISTS table_history" +
            "(history_id integer PRIMARY KEY NOT NULL,"+
            "history_name text NOT NULL," +
            "history_username text NOT NULL," +
            "history_time text NOT NULL);";
    private static final String CREATE_LINK_CLASS_USER = "CREATE TABLE IF NOT EXISTS link_class_user" +
            "(class_id integer NOT NULL," +
            "user_id integer NOT NULL," +
            "PRIMARY KEY (class_id, user_id)," +
            "FOREIGN KEY(class_id) REFERENCES table_classes(class_id)," +
            "FOREIGN KEY(user_id) REFERENCES table_users(user_id));";

    private static final String CREATE_LINK_GYM_USER = "CREATE TABLE IF NOT EXISTS link_gym_user" +
            "(gym_id integer NOT NULL," +
            "user_id integer NOT NULL," +
            "PRIMARY KEY (gym_id, user_id)," +
            "FOREIGN KEY(gym_id) REFERENCES table_gyms(gym_id)," +
            "FOREIGN KEY(user_id) REFERENCES table_users(user_id));";

    private static final String CREATE_LINK_GYM_CLASS = "CREATE TABLE IF NOT EXISTS link_gym_class" +
            "(gym_id integer NOT NULL," +
            "class_id integer NOT NULL," +
            "PRIMARY KEY (gym_id, class_id)," +
            "FOREIGN KEY(gym_id) REFERENCES table_gyms(gym_id)," +
            "FOREIGN KEY(class_id) REFERENCES table_classes(class_id));";

    private static final String CREATE_LINK_CLASS_TYPE = "CREATE TABLE IF NOT EXISTS link_class_type" +
            "(class_id integer NOT NULL," +
            "type_id integer NOT NULL," +
            "PRIMARY KEY (class_id, type_id)," +
            "FOREIGN KEY(class_id) REFERENCES table_classes(class_id), " +
            "FOREIGN KEY(type_id) REFERENCES table_types(type_id));";

    public DBHandler(Context context){
        super(context,DATABASE_NAME, null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        try {
            db.execSQL(CREATE_TABLE_USERS);
            db.execSQL(CREATE_TABLE_GYMS);
            db.execSQL(CREATE_TABLE_CLASSES);
            db.execSQL(CREATE_TABLE_TYPES);
            db.execSQL(CREATE_LINK_CLASS_USER);
            db.execSQL(CREATE_LINK_GYM_CLASS);
            db.execSQL(CREATE_LINK_GYM_USER);
            db.execSQL(CREATE_LINK_CLASS_TYPE);
            db.execSQL(CREATE_TABLE_HISTORY);
        }
        catch (Exception error){
            System.out.println("Database already exists");
        }
        this.db = db;
        populateDB();
    }

    public boolean insertData(User user){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME,user.getUserName());
        values.put(COLUMN_PASSWORD,user.getUserPass());
        values.put(COLUMN_EMAIL,user.getUserEmail());
        values.put(COLUMN_MOBILE,user.getUserPhone());
        values.put(COLUMN_PREFSPORT,user.getUserPrefSport());
        values.put(COLUMN_PREFCLASS,user.getUserPrefSport());
        try{
            db.insert(TABLE_NAME, null ,values);
            Log.d("Insert SUCCESS", values.toString());
            return true;
        } catch (Exception e){
            Log.d("Insert FAILURE", e.toString());
            return false;
        }
    }
    public boolean userExists(String user,String password){
        String fetchuser;
        String fncName = "userExists";
        String currentCursor = "na";
        if (password != null) {
            fetchuser = "SELECT username, password FROM table_users";
        }
        else{
            fetchuser = "select username from " + TABLE_NAME;
        }
        Cursor cursor = db.rawQuery(fetchuser, null);
        if (cursor.moveToFirst()){
            do{
                currentCursor = cursor.getString(0);
                if (currentCursor.equals(user)){
                    if (password == null){
                        return true;
                    }
                    else{
                        currentCursor = cursor.getString(1);
                        break;
                    }
                }
            }
            while (cursor.moveToNext());
        }
        if (currentCursor.equals(password)){
            return true;
        }
        else{
            return false;
        }
    }

    public String getUserName(String username){
        //db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT username FROM table_users", null);
        String currentUsername = "UserNotFound";
        Log.d("Cursor count", String.valueOf(cursor.getCount()));
        if (cursor.moveToFirst()){
            Log.d("Select ", " clause");
            do{
                currentUsername = cursor.getString(0);
                Log.d("currentUsername", currentUsername);
                if (currentUsername.equals(username)){
                    Log.d("currentUsername", "User Found!");
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return currentUsername;
    }

    public String getUserID(String username){
        String fetchUserID = "SELECT user_id FROM table_users WHERE username = '" + username + "';";
        Log.d("getUSerID", fetchUserID);
        Cursor cursor = db.rawQuery(fetchUserID, null);
        String currentCursor = "NoUser";
        if (cursor.moveToFirst()){
            currentCursor = cursor.getString(0);
        }
        return currentCursor;
    }

    public boolean modifyUserInfo(int modCase, String username, String newValue){
        uID = getUserID(username);
        String table = "table_users";
        String whereClause = "user_id = " + uID;
        Log.d("modifyUserInfo", "uID is " + uID);
        Log.d("modifyUserInfo", "modCase is " + String.valueOf(modCase));
        ContentValues columnAndNewValue = new ContentValues();
        switch (modCase){
            case 0: //modCase = username
                Log.d("modifyUserInfo", "Modifying username to " + newValue);
                columnAndNewValue.put("username", newValue);
                db.update(table, columnAndNewValue, whereClause, null);
                break;
            case 1: //modCase = password
                Log.d("modifyUserInfo", "Modifying password to " + newValue);
                columnAndNewValue.put("password", newValue);
                db.update(table, columnAndNewValue, whereClause, null);
                break;
            case 2: //modCase = email
                Log.d("modifyUserInfo", "Modifying email to " + newValue);
                columnAndNewValue.put("email", newValue);
                db.update(table, columnAndNewValue, whereClause, null);
                break;
            case 3: //modCase = phone
                Log.d("modifyUserInfo", "Modifying phone number to " + newValue);
                columnAndNewValue.put("mobile", newValue);
                db.update(table, columnAndNewValue, whereClause, null);
                break;
            case 4: //modCase = preferred class
                Log.d("modifyUserInfo", "Modifying preferred class to " + newValue);
                columnAndNewValue.put("pref_class", newValue);
                db.update(table, columnAndNewValue, whereClause, null);
                break;
            case 5: //modCase = preffered sport
                Log.d("modifyUserInfo", "Modifying preferred sport to " + newValue);
                columnAndNewValue.put("pref_sport", newValue);
                db.update(table, columnAndNewValue, whereClause, null);
                break;
            default:
                Log.d("modifyUserInfo", "modCase exceeds 5 or has not been given, doing nothing.");
                return false;
        }
        Log.d("modifyUserInfo", "Modified database with new data");
        return true;
    }

    public String getUserInfo(String username){
        String returnString = "Oops";
        uID = getUserID(username);
        Cursor cursor = db.rawQuery("SELECT username, email, mobile, pref_class, pref_sport FROM table_users WHERE user_id = " + uID, null);
        if (cursor.moveToFirst()){
            returnString = "Username: " + cursor.getString(0);
            returnString = returnString + "\n\nEmail: " + cursor.getString(1);
            returnString = returnString + "\n\nPhone: " + cursor.getString(2);
            returnString = returnString + "\n\nPreferred Class: " + cursor.getString(3);
            returnString = returnString + "\n\nPreferred Sport: " + cursor.getString(4);
        }
        return returnString;
    }

    public boolean deleteAccount(String username){
        Log.d("deleteAccount", username);
        uID = getUserID(username);
        Log.d("deleteAccount", "uID is " + uID);
        try {
            if (checkGymMemebership(username)) {
                Log.d("deleteAccount", "User has a link to a gym or gyms, removing");
                db.delete("link_gym_user", "user_id = " + uID, null);
            }
            if (checkClassMembership(username)){
                Log.d("deleteAccount", "User has a link to a class or classes, removing");
                db.delete("link_class_user", "user_id = " + uID, null);
            }
            if (checkUserHistory(username)){
                Log.d("deleteAccount", "User has history, removing");
                db.delete("table_history", "history_username = '" + username + "'", null);
            }
            Log.d("deleteAccount", "Deleting user");
            db.delete("table_users", "user_id = " + uID, null);
            return true;
        }
        catch (Exception error){
            Log.d("deleteAccount", "Error while deleting user");
            Log.d("deleteAccount", String.valueOf(error));
            return false;
        }
    }

    public String[] getTypes(Boolean getSports){
        if (!getSports){ //Get classes if getSports is false
            Log.d("getTypes", "Getting all classses");
            int x = 0;
            Cursor cursor = db.rawQuery("SELECT type_name FROM table_types WHERE type_isClass = 1", null);
            Log.d("getTypes", "Cursor count is " + String.valueOf(cursor.getCount()));
            String[] returnString = new String[cursor.getCount() - 1];
            if (cursor.moveToFirst()){
                while (x < cursor.getCount() && cursor.moveToNext()){
                    returnString[x] = cursor.getString(0);
                    x++;
                    Log.d("getTypes", "x is " + String.valueOf(x));
                }
            }
            return returnString;
        }
        else{
            Log.d("getTypes", "Getting all sports");
            int x = 0;
            Cursor cursor = db.rawQuery("SELECT type_name FROM table_types WHERE type_isSport = 1", null);
            Log.d("getTypes", "Cursor count is " + String.valueOf(cursor.getCount()));
            String[] returnString = new String[cursor.getCount() - 1];
            if (cursor.moveToFirst()){
                while (x < cursor.getCount() && cursor.moveToNext()){
                    returnString[x] = cursor.getString(0);
                    x++;
                    Log.d("getTypes", "x is " + String.valueOf(x));
                }
            }
            return returnString;
        }
    }

    public String[] getGyms(){
        Cursor cursor = db.rawQuery("SELECT gym_name FROM table_gyms", null);
        Log.d("getGyms", "Cursor count is " + String.valueOf(cursor.getCount()));
        String[] returnValues = new String[cursor.getCount()];
        int x = 0;
        if (cursor.moveToFirst()){
            do{
                returnValues[x] = cursor.getString(0);
                Log.d("getGyms", "Current gym is " + cursor.getString(0));
                x++;
                Log.d("getGyms", "x is " + String.valueOf(x));
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
        }
        return returnValues;
    }

    public String[] getGymInfo(String gymID){
        Log.d("getGymInfo", "Given ID is " + gymID);
        Cursor cursor = db.rawQuery("SELECT * FROM table_gyms WHERE gym_id = " + gymID, null);
        String[] returnValues = new String[5];
        int x = 0;
        if (cursor.moveToFirst()){
            while (x <= 4){
                returnValues[x] = cursor.getString(x + 1);
                x++;
                Log.d("getGymInfo", "x is " + String.valueOf(x));
            }
        }
        Log.d("getGymInfo", "Returning values");
        return returnValues;
    }

    public boolean checkGymMemebership(String username){
        uID = getUserID(username);
        Cursor cursor = db.rawQuery("SELECT user_id FROM link_gym_user WHERE user_id = " + uID, null);
        if (cursor.getCount() >= 1){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean checkIndividualGymMembership(String username, String gID){
        uID = getUserID(username);
        String query = "SELECT user_id FROM link_gym_user WHERE user_id = " + uID + " AND gym_id = " + gID;
        Log.d("checkIndividualGymMembership", query);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() >= 1){
            Log.d("checkIndividualGymMembership", "User is in specified gym");
            return true;
        }
        else{
            Log.d("checkIndividualGymMembership", "User is NOT in specified gym");
            return false;
        }
    }

    public boolean setGymMembership(String username, String gID){
        /*Sets a link between a user and their selected gym.
        Returns true if the link was CREATED
        and false if the lin was REMOVED*/
        uID = getUserID(username);
        if (checkIndividualGymMembership(username, gID)){
            Log.d("setGymMembership", "User is a member of the gym already, REMOVING their link");
            db.delete("link_gym_user", "user_id = " + uID + " AND gym_id = " + gID, null);
            return false;
        }
        else{
            Log.d("setGymMembership", "User is NOT a member of the gym, ADDING their link");
            ContentValues userLink = new ContentValues();
            userLink.put("gym_id", gID);
            userLink.put("user_id", uID);
            db.insert("link_gym_user", null, userLink);
            return true;
        }
    }

    public String[] listUsersGyms(String username){
        uID = getUserID(username);
        Cursor cursor = db.rawQuery("SELECT gym_id FROM link_gym_user WHERE user_id = " + uID, null);
        String[] cIDList = new String[cursor.getCount()];
        String[] returnValues = new String[cursor.getCount()];
        Log.d("listUsersGyms", "Cursor count is " + String.valueOf(cursor.getCount()));
        int x = 0;
        if (cursor.moveToFirst()){
            Log.d("listUsersGyms", "Adding gym IDs to cIDList");
            do{
                cIDList[x] = cursor.getString(0);
                x++;
                Log.d("listUsersGyms", "x is " + String.valueOf(x));
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
            Log.d("listUsersGyms", "Gym IDs added");
            Log.d("listUsersGyms", "Getting gym names from gym IDs");
            x = 0;
            do{
                Log.d("listUsersGyms", "x is " + String.valueOf(x));
                cursor = db.rawQuery("SELECT gym_name FROM table_gyms WHERE gym_id = " + cIDList[x], null);
                cursor.moveToFirst();
                returnValues[x] = cursor.getString(0);
                Log.d("listUsersGyms", "current cursor value is " + cursor.getString(0));
                x++;
            }
            while (x < cIDList.length);
            Log.d("listUsersGyms", "Returning gym names");
            return returnValues;
        }
        else{
            Log.d("listUsersGyms", "Users is not in any gyms!");
            return returnValues;
        }
    }

    public String[] getIndividualGymClasses(String gID){
        Cursor cID_cursor = db.rawQuery("SELECT class_id FROM link_gym_class WHERE gym_id = " + gID, null);
        Cursor returnCursor;
        String[] returnValues = new String[cID_cursor.getCount()];
        String query = "";
        int x = 0;
        if (cID_cursor.moveToFirst()){
            do{
                query = "SELECT class_name FROM table_classes WHERE class_id = " + cID_cursor.getString(0);
                Log.d("getIndividualGymClasses", "query = " + query);
                returnCursor = db.rawQuery(query, null);
                if (returnCursor.moveToFirst()){
                    Log.d("getIndividualGymClasses", "Retrieved class is " + returnCursor.getString(0));
                    returnValues[x] = returnCursor.getString(0);
                }
                x++;
                Log.d("getIndividualGymClasses", "x is " + String.valueOf(x));
            }
            while (cID_cursor.moveToNext() && x <= cID_cursor.getCount());
        }
        return returnValues;
    }

    public int getClassID(String className){
        Cursor cursor = db.rawQuery("SELECT class_id FROM table_classes WHERE class_name = '" + className + "'", null);
        if (cursor.moveToFirst()){
            return Integer.parseInt(cursor.getString(0));
        }
        else{
            return 0;
        }
    }

    public String[] getClassInfo(String classID){
        Cursor cursor = db.rawQuery("SELECT * FROM table_classes WHERE class_id = " + classID, null);
        String[] returnValues = new String[5];
        int x = 0;
        if (cursor.moveToFirst()){
            while (x <= 4){
                returnValues[x] = cursor.getString(x + 1);
                x++;
                Log.d("getClassInfo", "x is " + String.valueOf(x));
            }
        }
        Log.d("getClassInfo", "Returning values");
        return returnValues;
    }

    public boolean checkClassMembership(String username){
        uID = getUserID(username);
        Cursor cursor = db.rawQuery("SELECT user_id FROM link_class_user WHERE user_id = " + uID, null);
        if (cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean checkIndividualClassMembership(String username, String cID){
        uID = getUserID(username);
        String query = "SELECT user_id FROM link_class_user WHERE user_id = " + uID + " AND class_id = " + cID;
        Log.d("checkIndividualClassMembership", query);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() >= 1){
            Log.d("checkIndividualClassMembership", "User is in specified class");
            return true;
        }
        else{
            Log.d("checkIndividualClassMembership", "User is NOT in specified class");
            return false;
        }
    }

    public boolean checkClassSpace(String cID){
        /*Checks the capacity of the specified class against the amount of people in the class.
        If the amount is less than the capcity, this method will return true.
        If not, it will return false which means the class is full.*/
        Log.d("checkClassSpace", "Checking if class has space");
        String query = "SELECT class_capacity, class_amount FROM table_classes WHERE class_id = " + cID;
        Log.d("checkClassSpace", query);
        Cursor cursor = db.rawQuery(query, null);
        Log.d("checkClassSpace", "Cursor amount is " + String.valueOf(cursor.getCount()));
        Log.d("checkClassSpace", "Cursor column amount is " + String.valueOf(cursor.getColumnCount()));
        cursor.moveToFirst();
        Log.d("checkClassSpace", "Cursor Value 0 is " + cursor.getString(0) + "\nCursor Value 1 is " + cursor.getString(1));
        if (Integer.parseInt(cursor.getString(1)) < Integer.parseInt(cursor.getString(0))){
            Log.d("checkClassSpace", "Class has space for user");
            return true;
        }
        else{
            Log.d("checkClassSpace", "Class does NOT have space for user");
            return false;
        }
    }

    public int getClassAmount(String cID){
        int returnValue = 0;
        Cursor cursor = db.rawQuery("SELECT class_amount FROM table_classes WHERE class_id = " + cID, null);
        cursor.moveToFirst();
        returnValue = Integer.parseInt(cursor.getString(0));
        Log.d("getClassAmount", "returnValue is " + String.valueOf(returnValue));
        return returnValue;
    }

    public String getClassName(String cID){
        Cursor cursor = db.rawQuery("SELECT class_name FROM table_classes WHERE class_id = " + cID, null);
        String className = "NoClass";
        if (cursor.moveToFirst()){
            className = cursor.getString(0);
        }
        return className;
    }

    public boolean setClassMemebership(String username, String cID, boolean setCase){
        uID = getUserID(username);
        int cAmount = getClassAmount(cID);
        if (setCase){
            Log.d("setClassMembership", "Creating link between class and user");
            ContentValues userLink = new ContentValues();
            userLink.put("class_id", cID);
            userLink.put("user_id", uID);
            db.insert("link_class_user", null, userLink);
            cAmount++;
            Log.d("setClassMembership", "Link created, class amount is now " + String.valueOf(cAmount));
            userLink = null;
            userLink = new ContentValues();
            userLink.put("class_amount", String.valueOf(cAmount));
            Log.d("setClassMemebership", "Updating class amount in database");
            db.update("table_classes", userLink, "class_id = " + cID, null);
            return true;
        }
        else{
            Log.d("setClassMemebership", "Removing link between class and user");
            db.delete("link_class_user", "user_id = " + uID + " AND class_id = " + cID, null);
            cAmount--;
            if (cAmount < 0){
                cAmount = 0;
            }
            Log.d("setClassMembership", "Link removed, class amount is now " + String.valueOf(cAmount));
            ContentValues userLink = new ContentValues();
            userLink.put("class_amount", String.valueOf(cAmount));
            Log.d("setClassMemebership", "Updating class amount in database");
            db.update("table_classes", userLink, "class_id = " + cID, null);
            return false;
        }
    }

    public String[] searchGyms(String search){
        if (search.contains("'")){
            StringBuilder searchEscape = new StringBuilder(search);
            int escapeIndex = searchEscape.indexOf("'");
            searchEscape.insert(escapeIndex, "'");
            search = String.valueOf(searchEscape);
        }
        if (search.contains("\"")){
            StringBuilder searchEscape = new StringBuilder(search);
            int escapeIndex = searchEscape.indexOf("\"");
            searchEscape.insert(escapeIndex, "\"");
            search = String.valueOf(searchEscape);
        }
        String query = "SELECT gym_id, gym_name FROM table_gyms WHERE gym_name LIKE '" + search + "%'";
        Log.d("searchGyms", query);
        Cursor cursor = db.rawQuery(query, null);
        String[] returnValues = new String[cursor.getCount()];
        if (cursor.moveToFirst()){
            Log.d("searchGyms", "Found gyms that match search");
            int x = 0;
            do{
                Log.d("searchGyms", "x is " + String.valueOf(x));
                returnValues[x] = cursor.getString(1);
                returnValues[x] = returnValues[x] + "/" + cursor.getString(0);
                Log.d("searchGyms", "Current cursor is " + cursor.getString(1) + "\nWith ID of " + cursor.getString(0));
                Log.d("searchGyms", "returnValues[" + String.valueOf(x) + "] is " + returnValues[x]);
                x++;
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
        }
        else{
            Log.d("searchGyms", "No gyms found that match the search");
        }
        return returnValues;
    }

    public void addUserHistory(String username, String cID){
        Date dTime = Calendar.getInstance().getTime();
        DateFormat dTimeFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String currentTime = dTimeFormat.format(dTime);
        String classname = getClassName(cID);
        ContentValues dbData = new ContentValues();
        dbData.put("history_name", classname);
        dbData.put("history_username", username);
        dbData.put("history_time", currentTime);
        db.insert("table_history", null, dbData);
    }

    public String[] readUserHistory(String username){
        Log.d("readUserHistory", "username is " + username);
        Cursor cursor = db.rawQuery("SELECT history_name FROM table_history WHERE history_username = '" + username + "'", null);
        String[] returnValues = new String[cursor.getCount()];
        Log.d("readUserHistory", "Cursor count is " + String.valueOf(cursor.getCount()));
        if (cursor.moveToFirst()){
            Log.d("readUserHistory", "User has past classes");
            int x = 0;
            do{
                Log.d("readUserHistory", "x is " + String.valueOf(x));
                Log.d("readUserHistory", "Current class is " + cursor.getString(0));
                returnValues[x] = cursor.getString(0);
                x++;
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
        }
        else{
            Log.d("readUserHistory", "User does NOT have past classes");
        }
        return returnValues;
    }

    public boolean checkUserHistory(String username){
        Log.d("checkUserHistory", "Checking if user has history");
        Cursor cursor = db.rawQuery("SELECT history_id FROM table_history WHERE history_username = '" + username + "'", null);
        if (cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    public void clearUserHistory(String username){
        Log.d("clearUserHistory", "Deleting user's class history");
        db.delete("table_history", "history_username = '" + username +"'", null);
    }

    public void postTest(){
        db.delete("link_class_type", "1=1", null);
        db.delete("link_class_user", "1=1", null);
        db.delete("link_gym_class", "1=1", null);
        db.delete("link_gym_user", "1=1", null);
        db.delete("table_classes", "1=1", null);
        db.delete("table_gyms", "1=1", null);
        db.delete("table_history", "1=1", null);
        db.delete("table_types", "1=1", null);
        db.delete("table_users", "1=1", null);
    }

    public void populateDB(){
        db.delete("table_types", "1=1", null);
        db.delete("table_gyms", "1=1", null);
        db.delete("table_classes", "1=1", null);
        ContentValues dbData = new ContentValues();
        //Table type data
        dbData.put("type_name", "Burn It / HIIT"); //https://www.puregym.com/classes/categories/cardio/
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Cycle / Spin");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Combat");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Circuit");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Step");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Box Fit");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Aerobics");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Zumba");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        //https://www.puregym.com/classes/categories/sculpt-tone/
        dbData.put("type_name", "Pump");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Body Tone");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        //https://www.puregym.com/classes/categories/mind-body/
        dbData.put("type_name", "Yoga");
        dbData.put("type_isClass", true);
        dbData.put("type_isSport", false);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Gymnastics");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Target Archery");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Dodgeball");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Abseiling");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Rock Climbing");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Wrestling");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Karate");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Parkour");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Running");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Clay Pidgeon Shooting");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Laser Tag");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Paintball");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Football");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Hockey");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Badminton");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Tennis");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Cricket");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Weightlifting");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Baseball");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        dbData.put("type_name", "Boxing");
        dbData.put("type_isClass", false);
        dbData.put("type_isSport", true);
        db.insert("table_types", null, dbData);
        //Table gym data
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_name", "Bobby Gym");
        dbData.put("gym_location", "LS1");
        dbData.put("gym_cost", 30);
        dbData.put("gym_time_open", "0600");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "ImpureGym");
        dbData.put("gym_location", "LS1");
        dbData.put("gym_cost", 40);
        dbData.put("gym_time_open", "0800");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "Metamorph");
        dbData.put("gym_location", "LS3");
        dbData.put("gym_cost", 40);
        dbData.put("gym_time_open", "0800");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "Iron Noodle");
        dbData.put("gym_location", "LS6");
        dbData.put("gym_cost", 40);
        dbData.put("gym_time_open", "0800");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "ThunderGym");
        dbData.put("gym_location", "LS12");
        dbData.put("gym_cost", 40);
        dbData.put("gym_time_open", "0800");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "PowerUp");
        dbData.put("gym_location", "LS8");
        dbData.put("gym_cost", 50);
        dbData.put("gym_time_open", "0600");
        dbData.put("gym_time_close", "2300");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "BodyPlus");
        dbData.put("gym_location", "LS15");
        dbData.put("gym_cost", 20);
        dbData.put("gym_time_open", "1500");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        dbData.put("gym_name", "ReSync Gym");
        dbData.put("gym_location", "LS1");
        dbData.put("gym_cost", 80);
        dbData.put("gym_time_open", "0000");
        dbData.put("gym_time_close", "0000");
        db.insert("table_gyms", null, dbData);
        //Class data
        //Bobby Gym classes
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Bobby Spin Class");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "1600");
        dbData.put("class_capacity", 10);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 1);
        dbData.put("class_id", 1);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Bobby Yoga");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "1200");
        dbData.put("class_capacity", 10);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 1);
        dbData.put("class_id", 2);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Bobby HIIT IT");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "1800");
        dbData.put("class_capacity", 10);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 1);
        dbData.put("class_id", 3);
        db.insert("link_gym_class", null, dbData);
        //ImpureGym classes
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Impure Aerobics");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "1800");
        dbData.put("class_capacity", 10);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 2);
        dbData.put("class_id", 4);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Impure Body Tone");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "2000");
        dbData.put("class_capacity", 10);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 2);
        dbData.put("class_id", 5);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Impure Yoga");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "2300");
        dbData.put("class_capacity", 10);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 2);
        dbData.put("class_id", 6);
        db.insert("link_gym_class", null, dbData);
        //BodyPlus classes
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "YogaPlus");
        dbData.put("class_cost", 40);
        dbData.put("class_time", "1200");
        dbData.put("class_capacity", 20);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 7);
        dbData.put("class_id", 7);
        db.insert("link_gym_class", null, dbData);
        //ResyncGym classes
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Resync Spin");
        dbData.put("class_cost", 30);
        dbData.put("class_time", "1300");
        dbData.put("class_capacity", 32);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 8);
        dbData.put("class_id", 8);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Resync Combat Training");
        dbData.put("class_cost", 60);
        dbData.put("class_time", "0800");
        dbData.put("class_capacity", 8);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 8);
        dbData.put("class_id", 9);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Resync Circuits");
        dbData.put("class_cost", 20);
        dbData.put("class_time", "1400");
        dbData.put("class_capacity", 16);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 8);
        dbData.put("class_id", 10);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Resync Yoga");
        dbData.put("class_cost", 10);
        dbData.put("class_time", "1100");
        dbData.put("class_capacity", 32);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 8);
        dbData.put("class_id", 11);
        db.insert("link_gym_class", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("class_name", "Resync Zumba");
        dbData.put("class_cost", 30);
        dbData.put("class_time", "1400");
        dbData.put("class_capacity", 16);
        dbData.put("class_amount", 0);
        db.insert("table_classes", null, dbData);
        dbData = null;
        dbData = new ContentValues();
        dbData.put("gym_id", 8);
        dbData.put("class_id", 12);
        db.insert("link_gym_class", null, dbData);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropquery= "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(dropquery);
        this.onCreate(db);
    }

}

package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MyGymsActivity extends AppCompatActivity {

    String username;
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_gyms);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("OnCreate", username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        myGymList();
    }

    public void myGymList(){
        String[] gymList = handler.listUsersGyms(username);
        LinearLayout myGymListScroll = findViewById(R.id.app_mygym_linear_myGymScroll);
        if (gymList.length > 0){
            Log.d("myGymList", "Gyms found!");
            TextView[] gymNames = new TextView[gymList.length];
            int x = 0;
            while (x < gymList.length){
                Log.d("myGymList", "x is " + String.valueOf(x));
                Log.d("myGymList", "Current gym name is " + gymList[x]);
                gymNames[x] = new TextView(this);
                gymNames[x].setText(gymList[x]);
                gymNames[x].setGravity(Gravity.CENTER);
                gymNames[x].setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
                myGymListScroll.addView(gymNames[x]);
                x++;
            }
        }
        else{
            Log.d("myGymList", "No gyms found!");
            TextView noGyms = new TextView(this);
            noGyms.setText(getString(R.string.app_mygym_textview_noGyms));
            noGyms.setGravity(Gravity.CENTER);
            noGyms.setTextColor(Color.RED);
            noGyms.setTypeface(Typeface.DEFAULT_BOLD);
            noGyms.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            myGymListScroll.addView(noGyms);
        }
    }

    public void backButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}
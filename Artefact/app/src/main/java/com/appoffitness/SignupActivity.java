package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        //MainActivity.getInstance().finish();
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }

    public void signUpButton(View view){
        EditText userEntry = findViewById(R.id.app_signup_entry_username);
        EditText passEntry = findViewById(R.id.app_signup_entry_password);
        EditText emailEntry = findViewById(R.id.app_signup_entry_email);
        EditText phoneEntry = findViewById(R.id.app_signup_entry_phone);
        if (String.valueOf(userEntry.getText()).equals("")){
            setConstraintError(view, false);
            userEntry.setError(getResources().getString(R.string.app_error_userempty));
            userEntry.requestFocus();
            return;
        }
        else if (String.valueOf(passEntry.getText()).equals("") || String.valueOf(passEntry.getText()).length() < 8){
            setConstraintError(view, false);
            if (String.valueOf(passEntry.getText()).equals("")) {
                passEntry.setError(getResources().getString(R.string.app_error_passempty));
            }
            else if (String.valueOf(passEntry.getText()).length() < 8){
                passEntry.setError(getResources().getString(R.string.app_error_passshort));
            }
            passEntry.requestFocus();
            return;
        }
        else if (String.valueOf(emailEntry.getText()).equals("") || !String.valueOf(emailEntry.getText()).contains("@")) {
            setConstraintError(view, true);
            if (String.valueOf(emailEntry.getText()).equals("")) {
                emailEntry.setError(getResources().getString(R.string.app_error_emailempty));
            }
            else if (!String.valueOf(emailEntry.getText()).contains("@")){
                emailEntry.setError(getResources().getString(R.string.app_error_notemail));
            }
            emailEntry.requestFocus();
            return;
        }
        else{
            if (handler.userExists(String.valueOf(userEntry.getText()), null)){
                Toast.makeText(this, "User already exists!", Toast.LENGTH_LONG).show();
            }
            else {
                String phoneText = String.valueOf(phoneEntry.getText());
                if (phoneText.equals("")) {
                    phoneText.equals("NONE");
                }
                User user = new User(String.valueOf(userEntry.getText()), String.valueOf(passEntry.getText()), String.valueOf(emailEntry.getText()), phoneText, "None", "None", true);
                handler.insertData(user);
                backButton(view);
            }
        }
    }

    //Constraint switch. Switches between the two constraints.
    // <The first being for the username and password and the second being for other info and having the create account button>
    public void constraintSwitch(View view){
        ConstraintLayout startConstraint = findViewById(R.id.app_signup_constraint_start);
        ConstraintLayout endConstraint = findViewById(R.id.app_signup_constraint_end);
        if (startConstraint.getVisibility() == view.GONE){
            startConstraint.setVisibility(view.VISIBLE);
            endConstraint.setVisibility(view.GONE);
        }
        else{
            startConstraint.setVisibility(view.GONE);
            endConstraint.setVisibility(view.VISIBLE);
        }
    }

    /*This switches between the two constraints when there's an error
    to focus on the textbox that caused it. If reverse is true, it sets the end
    constraint to be visible else, it sets the start constraint to be visible.*/
    public void setConstraintError(View view, Boolean reverse){
        ConstraintLayout startConstraint = findViewById(R.id.app_signup_constraint_start);
        ConstraintLayout endConstraint = findViewById(R.id.app_signup_constraint_end);
        if (!reverse){
            if (startConstraint.getVisibility() == view.GONE){
                startConstraint.setVisibility(view.VISIBLE);
                endConstraint.setVisibility(view.GONE);
            }
        }
        else{
            if (endConstraint.getVisibility() == view.GONE){
                endConstraint.setVisibility(view.VISIBLE);
                startConstraint.setVisibility(view.GONE);
            }
        }
    }

    //Back button to main screen.
    public void backButton(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Intent backButtonIntent = new Intent(this, MainActivity.class);
        startActivity(backButtonIntent);
        finish();
    }
}
package com.appoffitness;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }

    public void loginButton(View view){
        EditText userEntry = findViewById(R.id.app_login_entry_username);
        EditText passEntry = findViewById(R.id.app_login_entry_password);
        if (String.valueOf(userEntry.getText()).equals("")){
            userEntry.setError(getResources().getString(R.string.app_error_userempty));
            userEntry.requestFocus();
            return;
        }
        if (String.valueOf(passEntry.getText()).equals("")){
            passEntry.setError(getResources().getString(R.string.app_error_passempty));
            passEntry.requestFocus();
            return;
        }
        String username = String.valueOf(userEntry.getText());
        String password = String.valueOf(passEntry.getText());
        if (handler.userExists(username, password)){
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            String uname = handler.getUserName(username);
            Intent loginSwitch = new Intent(this, HomeActivity.class);
            loginSwitch.putExtra("username", uname);
            loginSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginSwitch);
            finish();
        }
        else{
            Toast.makeText(this, "Incorrect Username or Password", Toast.LENGTH_LONG).show();
        }
    }

    public void backButton(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Intent backButtonIntent = new Intent(this, MainActivity.class);
        startActivity(backButtonIntent);
        finish();
    }
}
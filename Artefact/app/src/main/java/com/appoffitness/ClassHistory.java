package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ClassHistory extends AppCompatActivity {

    String username;
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_history);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("CHistoryOnCreate", username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        listHistory();
    }

    public void listHistory(){
        Log.d("listHistory", "Getting user's history");
        Log.d("listHistory", "Username is " + username);
        String[] historyList = handler.readUserHistory(username);
        LinearLayout historyLinear = findViewById(R.id.app_classhistory_linear_pastClassListScroll);
        if (historyList.length > 0){
            Log.d("listHistory", "User has past classes");
            TextView[] historyNames = new TextView[historyList.length];
            int x = 0;
            while (x < historyList.length){
                Log.d("listHistory", "x is " + String.valueOf(x));
                Log.d("listHistory", "Current history value " + historyList[x]);
                historyNames[x] = new TextView(this);
                historyNames[x].setText(historyList[x]);
                historyNames[x].setGravity(Gravity.CENTER);
                historyNames[x].setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);;
                historyLinear.addView(historyNames[x]);
                x++;
            }
        }
        else{
            Log.d("listHistory", "User has no past classes");
            if (historyLinear.getChildCount() > 0){
                historyLinear.removeAllViews();
            }
            TextView noClasses = new TextView(this);
            noClasses.setText(getString(R.string.app_classhistory_textview_nohistory));
            noClasses.setGravity(Gravity.CENTER);
            noClasses.setTextColor(Color.RED);
            noClasses.setTypeface(Typeface.DEFAULT_BOLD);
            noClasses.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            historyLinear.addView(noClasses);
        }
    }

    public void clearHistoryButton(View view){
        handler.clearUserHistory(username);
        listHistory();
    }

    public void backButton(View view){
        Intent homeSwitch = new Intent(this, AccountActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}
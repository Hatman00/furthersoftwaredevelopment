package com.appoffitness;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("OnCreate", username);
        TextView userWelcome = findViewById(R.id.app_home_textview_welcomeuser);
        String welcomeText = getResources().getString(R.string.app_home_textview_welcome) + "\n" + username + "!";
        userWelcome.setText(welcomeText);
    }

    public void gymSwitch(View view){
        Intent gymIntent = new Intent(this, GymActivity.class);
        gymIntent.putExtra("username", username);
        startActivity(gymIntent);
    }

    public void accountSettingsSwitch(View view){
        Intent accountSwitch = new Intent(this, AccountActivity.class);
        accountSwitch.putExtra("username", username);
        startActivity(accountSwitch);
    }

    public void myGymsSwitch(View view){
        Intent mySwitch = new Intent(this, MyGymsActivity.class);
        mySwitch.putExtra("username", username);
        startActivity(mySwitch);
    }

    public void gymSearchSwitch(View view){
        Intent searchSwitch = new Intent(this, GymSearch.class);
        searchSwitch.putExtra("username", username);
        startActivity(searchSwitch);
    }

    public void logoutButton(View view){
        Log.d("LOGOUT", "LOGGING OUT");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Intent logoutSwitch = new Intent(this, MainActivity.class);
        startActivity(logoutSwitch);
        finish();
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d("STOP!", "I HAVE STOPPED!");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("username", username);
        edit.apply();
    }
}
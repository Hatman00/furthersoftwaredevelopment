package com.appoffitness;

import static org.junit.Assert.*;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.test.platform.app.InstrumentationRegistry;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class DBHandlerTest{

    DBHandler handler;

    private void dbStart(){
        handler = new DBHandler(InstrumentationRegistry.getInstrumentation().getTargetContext());
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }

    private void testUser(){
        User testUser = new User("testUser", "testPassword", "test@user.com", "testNumber", "NONE", "NONE", true);
        handler.insertData(testUser);
    }

    @Test
    public void insertData() {
        dbStart();
        User testUser = new User("testUser", "testPassword", "test@user.com", "testNumber", "NONE", "NONE", true);
        assertTrue(handler.insertData(testUser));
        handler.postTest();
        handler = null;
    }

    @Test
    public void userExists() {
        dbStart();
        testUser();
        assertTrue(handler.userExists("testUser", "testPassword"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getUserName() {
        dbStart();
        testUser();
        assertEquals("testUser", handler.getUserName("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getUserID() {
        dbStart();
        testUser();
        assertEquals("1", handler.getUserID("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void modifyUserInfo() {
        dbStart();
        testUser();
        assertTrue(handler.modifyUserInfo(0, "testUser", "userTest"));
        assertEquals("userTest", handler.getUserName("userTest"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getUserInfo() {
        dbStart();
        testUser();
        assertTrue(handler.getUserInfo("testUser").contains("test@user.com"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void deleteAccount() {
        dbStart();
        testUser();
        assertTrue(handler.deleteAccount("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getTypes() {
        dbStart();
        String[] testStringArray = handler.getTypes(false);
        String[] actualStringArray = {"Cycle / Spin", "Combat", "Circuit", "Step", "Box Fit", "Aerobics", "Zumba", "Pump", "Body Tone", "Yoga"};
        int x = 0;
        while (x < testStringArray.length){
            Log.d("getTypesTest", "x is " + String.valueOf(x) + "\ntestStringArray is " + testStringArray[x] + "\nactualStringArray is " + actualStringArray[x]);
            assertEquals(actualStringArray[x], testStringArray[x]);
            x++;
        }
        handler.postTest();
        handler = null;
    }

    @Test
    public void getGyms() {
        dbStart();
        String[] testStringArray = handler.getGyms();
        String[] actualStringArray = {"Bobby Gym", "ImpureGym", "Metamorph", "Iron Noodle", "ThunderGym", "PowerUp", "BodyPlus", "ReSync Gym"};
        int x = 0;
        while (x < testStringArray.length){
            Log.d("getGymsTest", "x is " + String.valueOf(x) + "\ntestStringArray is " + testStringArray[x] + "\nactualStringArray is " + actualStringArray[x]);
            assertEquals(actualStringArray[x], testStringArray[x]);
            x++;
        }
        handler.postTest();
        handler = null;
    }

    @Test
    public void getGymInfo() {
        dbStart();
        String[] testStringArray = handler.getGymInfo("1");
        String[] actualStringArray = {"Bobby Gym", "LS1", "30", "0600", "0000"};
        int x = 0;
        while (x < testStringArray.length){
            Log.d("getGymInfoTest", "x is " + String.valueOf(x) + "\ntestStringArray is " + testStringArray[x] + "\nactualStringArray is " + actualStringArray[x]);
            assertEquals(actualStringArray[x], testStringArray[x]);
            x++;
        }
        handler.postTest();
        handler = null;
    }

    @Test
    public void checkGymMemebership() {
        dbStart();
        testUser();
        handler.setGymMembership("testUser", "1");
        assertTrue(handler.checkGymMemebership("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void checkIndividualGymMembership() {
        dbStart();
        testUser();
        handler.setGymMembership("testUser", "1");
        assertTrue(handler.checkIndividualGymMembership("testUser", "1"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void setGymMembership() {
        dbStart();
        testUser();
        assertTrue(handler.setGymMembership("testUser", "1"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void listUsersGyms() {
        dbStart();
        testUser();
        handler.setGymMembership("testUser", "1");
        handler.setGymMembership("testUser", "2");
        String[] testStringArray = handler.listUsersGyms("testUser");
        String[] actualStringArray = {"Bobby Gym", "ImpureGym"};
        int x = 0;
        while (x < testStringArray.length){
            Log.d("listUserGymsTest", "x is " + String.valueOf(x) + "\ntestStringArray is " + testStringArray[x] + "\nactualStringArray is " + actualStringArray[x]);
            assertEquals(actualStringArray[x], testStringArray[x]);
            x++;
        }
        handler.postTest();
        handler = null;
    }

    @Test
    public void getIndividualGymClasses() {
        dbStart();
        String[] testStringArray = handler.getIndividualGymClasses("1");
        String[] actualStringArray = {"Bobby Spin Class", "Bobby Yoga", "Bobby HIIT IT"};
        int x = 0;
        while (x < testStringArray.length){
            Log.d("getIndividualGymClassesTest", "x is " + String.valueOf(x) + "\ntestStringArray is " + testStringArray[x] + "\nactualStringArray is " + actualStringArray[x]);
            assertEquals(actualStringArray[x], testStringArray[x]);
            x++;
        }
        handler.postTest();
        handler = null;
    }

    @Test
    public void getClassID() {
        dbStart();
        assertEquals(1, handler.getClassID("Bobby Spin Class"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getClassInfo() {
        dbStart();
        String[] testStringArray = handler.getClassInfo("1");
        String[] actualStringArray = {"Bobby Spin Class", "10", "1600", "10", "0"};
        int x = 0;
        while (x < testStringArray.length){
            Log.d("getClassInfoTest", "x is " + String.valueOf(x) + "\ntestStringArray is " + testStringArray[x] + "\nactualStringArray is " + actualStringArray[x]);
            assertEquals(actualStringArray[x], testStringArray[x]);
            x++;
        }
        handler.postTest();
        handler = null;
    }

    @Test
    public void checkClassMembership() {
        dbStart();
        testUser();
        handler.setClassMemebership("testUser", "1", true);
        assertTrue(handler.checkClassMembership("testUser"));
        handler.setClassMemebership("testUser", "1", false);
        assertFalse(handler.checkClassMembership("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void checkIndividualClassMembership() {
        dbStart();
        testUser();
        handler.setClassMemebership("testUser", "1", true);
        assertTrue(handler.checkIndividualClassMembership("testUser", "1"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void checkClassSpace() {
        dbStart();
        assertTrue(handler.checkClassSpace("1"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getClassAmount() {
        dbStart();
        assertEquals(0, handler.getClassAmount("1"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void getClassName() {
        dbStart();
        assertEquals("Bobby Spin Class", handler.getClassName("1"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void setClassMemebership() {
        dbStart();
        testUser();
        assertTrue(handler.setClassMemebership("testUser", "1", true));
        assertFalse(handler.setClassMemebership("testUser", "1", false));
        handler.postTest();
        handler = null;
    }

    @Test
    public void searchGyms() {
        dbStart();
        String[] testStringArray = handler.searchGyms("Bob");
        assertEquals("Bobby Gym/1", testStringArray[0]);
        handler.postTest();
        handler = null;
    }

    @Test
    public void addUserHistory() {
        dbStart();
        testUser();
        handler.addUserHistory("testUser", "1");
        assertTrue(handler.checkUserHistory("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void readUserHistory() {
        dbStart();
        testUser();
        handler.addUserHistory("testUser", "1");
        String[] testStringArray = handler.readUserHistory("testUser");
        assertEquals("Bobby Spin Class", testStringArray[0]);
        handler.postTest();
        handler = null;
    }

    @Test
    public void checkUserHistory() {
        dbStart();
        testUser();
        assertFalse(handler.checkUserHistory("testUser"));
        handler.addUserHistory("testUser", "1");
        assertTrue(handler.checkUserHistory("testUser"));
        handler.postTest();
        handler = null;
    }

    @Test
    public void clearUserHistory() {
        dbStart();
        testUser();
        handler.addUserHistory("testUser", "1");
        handler.clearUserHistory("testUser");
        assertFalse(handler.checkUserHistory("testUser"));
        handler.postTest();
        handler = null;
    }
}